﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

using PFCDAL;
using PFCInterfaces;

namespace PFCConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {

                PFCWrappedContext context = PFCWrappedContext.GetInstance();

                MainMessage();
                String answer = Console.In.ReadLine();

                Int32 answerValidation = -1;

                Int32.TryParse(answer,out answerValidation);

                if (answerValidation.Equals(1))
                {
                    SearchByEmployeeId();
                }
                else if (answerValidation.Equals(2))
                {
                    SearchByEmployeeSalary();
                }
                else if (answerValidation.Equals(3))
                {
                    Environment.Exit(0);
                }
                else if (answerValidation.Equals(4))
                {
                    DescribePettingZoo();
                }
                else
                {
                    Console.Clear();
                    Main(args);
                }
            }
            catch (Exception ex)
            {
                //all exception role up to here.
                Console.Out.WriteLine(ex.Message);

                Console.Out.WriteLine(ex.StackTrace);
                Console.Out.WriteLine("Press enter when done.");
                Console.ReadLine();
            }
        }

        private static void DescribePettingZoo()
        {
            List<Object> animals = new List<object>();

            Horses h = new Horses();
            Lizzard l = new Lizzard();
            Monkey m = new Monkey();
            WaterPark wp = new WaterPark();
            GiftShop gs = new GiftShop();

            animals.Add(h);
            animals.Add(l);
            animals.Add(m);
            animals.Add(wp);
            animals.Add(gs);

            Console.Out.WriteLine("Be sure to see these animals and by there food:");

            foreach (Object o in animals)
            {
                if(o.GetType().GetInterfaces().Contains(typeof(IAnimal)))
                {
                    Console.Out.WriteLine("Visit the: {0} and for {1} youll get {2} to feed the {0}",
                        o.GetType().Name.ToString(), 
                        ((IAnimal)o).GetFoodCost().ToString(), 
                        ((IAnimal)o).GetFoodDescription());
                }
            }
            Console.Out.WriteLine("After the petting zoo, dump the kids off here with more money:");

            foreach (Object o in animals)
            {
                if (!o.GetType().GetInterfaces().Contains(typeof(IAnimal)))
                {
                    Console.Out.WriteLine(o.ToString());
                }
            }
            Console.In.Read();
            Main(new string[] { });
        }

        public static void MainMessage()
        {
            Console.Out.WriteLine("Press 1 For Employees by ID\nPress 2 For Employees by Salary Greater Than\nPress 3 to Exit\nPress 4 for the interface examples");

        }

        public static void SearchByEmployeeId()
        {
            Console.Out.WriteLine("Enter the employee Id");

            String s = Console.ReadLine();

            Int64 validationValue = -1;

            if (!Int64.TryParse(s, out validationValue))
            {
                Console.Out.WriteLine("Please enter a single whole real number");
                SearchByEmployeeId();
            }
            else
            {
                PFCWrappedContext context = PFCWrappedContext.GetInstance();
                List<Employee> emps = context.GetEmployeeById(validationValue).ToList();

                foreach (Employee e in emps)
                {
                    Console.Out.WriteLine("First Name\tLast Name\tBirth Date\tSSN\t");

                    //I could write linq here to look for the max date or null in fired date of the related collection, depending on rules, but
                    //for demo Im just getting out the converted SSN.

                    String convSSN = e.EmployeeStats.First().ConvertedSSN;

                    Console.Out.WriteLine(e.FirstName + "\t" + e.LastName + "\t" + e.DateOfBirth.ToShortDateString() + "\t" + convSSN );
                }

                Console.Out.WriteLine("Hit enter to go back home");
                Console.ReadLine();
                Main(new String[]{});
            }

        }

        public static void SearchByEmployeeSalary()
        {
            Console.Out.WriteLine("Enter the Salary");

            String s = Console.ReadLine();

            Decimal validationValue = -1;

            if (!Decimal.TryParse(s, out validationValue))
            {
                Console.Out.WriteLine("Please enter a number in the form like: 65535.00");
                SearchByEmployeeSalary();
            }
            else
            {
                PFCWrappedContext context = PFCWrappedContext.GetInstance();
                List<Employee> emps = context.GetEmployeeBySalary(validationValue).ToList();

                foreach (Employee e in emps)
                {
                    Console.Out.WriteLine("First Name\tLast Name\tBirth Date\tSalary\t");

                    //I could write linq here to look for the max date or null in fired date of the related collection, depending on rules, but
                    //for demo Im just getting out the converted SSN.

                    String convSal = e.EmployeeStats.First().ConvertedSalary.ToString();

                    Console.Out.WriteLine(e.FirstName + "\t" + e.LastName + "\t" + e.DateOfBirth.ToShortDateString() + "\t" + convSal);
                }

                Console.Out.WriteLine("Hit enter to go back home");
                Console.ReadLine();
                Main(new String[] { });
            }
        }
    }
}
