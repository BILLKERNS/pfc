﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFCInterfaces
{
    public class GiftShop
    {
        public override string ToString()
        {
            return "Make sure to visit the gift shop where you can get pictures of your favorite animals";
        }
    }
}
