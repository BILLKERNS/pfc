﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFCInterfaces
{
    public interface IAnimal
    {

        Decimal GetFoodCost();

        String GetFoodDescription();
    }
}
