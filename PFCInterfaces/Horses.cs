﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFCInterfaces
{
    public class Horses : IAnimal
    {

        public decimal GetFoodCost()
        {
            return 2.5m;
        }

        public string GetFoodDescription()
        {
            return "Apples and Sugar Cubes for Horses";
        }

        
    }
}
