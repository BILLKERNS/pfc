﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFCInterfaces
{
    public class Lizzard : IAnimal
    {
        public decimal GetFoodCost()
        {
            return 2.75m;
        }

        public string GetFoodDescription()
        {
            return "Lettuce and Carrots for our cold bloded reptiles";
        }
    }
}
