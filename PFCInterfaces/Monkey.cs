﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFCInterfaces
{
    public class Monkey : IAnimal
    {
        public decimal GetFoodCost()
        {
            return .99m;
        }

        public string GetFoodDescription()
        {
            return "Every primates favorte source of B vitamins. . . Banannas";
        }
    }
}
