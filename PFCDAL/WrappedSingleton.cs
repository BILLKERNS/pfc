﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Configuration;


namespace PFCDAL
{
    public class PFCWrappedContext : DbContext
    {

        private PFCEntities _localEntities;
        private static PFCWrappedContext _singletonInstance;

        public static PFCWrappedContext GetInstance()
        {
            try
            {
                if (_singletonInstance == null)
                {
                    _singletonInstance = new PFCWrappedContext();
                    return _singletonInstance;
                }
                else
                {
                    return _singletonInstance;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private PFCWrappedContext()
        {
            try
            {
                
               
                _localEntities = new PFCEntities(ConfigurationManager.ConnectionStrings["PFCEntities"].ToString());

                foreach (EmployeeStat stat in _localEntities.EmployeeStats)
                {
                    SqlConnection connec = null;
                    try
                    {
                        connec = new SqlConnection("Data Source=.\\sqlexpress;Initial Catalog=PFC;Integrated Security=True");
                        connec.Open();


                        SqlCommand decrypSSN = new SqlCommand("DecryptSSN", connec);
                        decrypSSN.CommandType = System.Data.CommandType.StoredProcedure;
                        stat.InitializeDeCryptSSN(decrypSSN);

                        SqlCommand decrypSalary = new SqlCommand("DecryptSalary", connec);
                        decrypSalary.CommandType = System.Data.CommandType.StoredProcedure;
                        stat.InitializeDeCryptSalary(decrypSalary);

                        connec.Close();
                        connec.Dispose();


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        if (connec != null)
                        {
                            connec.Close();
                            connec.Dispose();
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        void _localEntities_ObjectMaterialized(object sender, System.Data.Objects.ObjectMaterializedEventArgs e)
        {//this wokes in EF 6
            if (e.Entity.GetType().Equals(typeof(EmployeeStat)))
            {
               
            }
        }

        public IEnumerable<Employee> GetEmployeeById(Int64 empId)
        {
            try
            {
                IEnumerable<Employee> employees = from Employee e in _localEntities.Employee
                                                  where e.PrimaryId.Equals(empId)
                                                  select e;
                return employees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Employee> GetEmployeeBySalary(Decimal greaterThan)
        {
            try
            {
                List<Employee> empsToReturn = new List<Employee>();
                foreach (EmployeeStat stat in _localEntities.EmployeeStats)
                {
                    if (stat.ConvertedSalary > greaterThan)
                    {
                        if (!empsToReturn.Contains(stat.Employee))
                        {
                            empsToReturn.Add(stat.Employee);
                        }
                    }
                }
                return empsToReturn.AsEnumerable<Employee>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
