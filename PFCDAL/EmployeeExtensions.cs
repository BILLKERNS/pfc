﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace PFCDAL
{
    public partial class EmployeeStat
    {

        public Decimal ConvertedSalary { get; set; }

        public String ConvertedSSN { get; set; }

    }

    public static class EmployeeStatsExtensions
    {
        public static void InitializeDeCryptSSN(this EmployeeStat empStat, SqlCommand procCommand)
        {
            SqlParameter paramIn = new SqlParameter();
            
            paramIn.ParameterName = "@EncryptedSSN";
            paramIn.SqlDbType = System.Data.SqlDbType.VarBinary;
            paramIn.Size = 128;
            paramIn.Direction = System.Data.ParameterDirection.Input;
            paramIn.Value = empStat.SSN;


            SqlParameter paramOut = new SqlParameter();
            paramOut.ParameterName = "@SSN";
            paramOut.SqlDbType = System.Data.SqlDbType.VarChar;
            paramOut.Size = 10;
            paramOut.Direction = System.Data.ParameterDirection.Output;


            procCommand.Parameters.Add(paramIn);
            procCommand.Parameters.Add(paramOut);

            procCommand.ExecuteNonQuery();

            empStat.ConvertedSSN = paramOut.Value.ToString();

        }

        public static void InitializeDeCryptSalary(this EmployeeStat empStat, SqlCommand procCommand)
        {
            SqlParameter paramIn = new SqlParameter();
            paramIn.ParameterName = "@EncryptedSalary";
            paramIn.SqlDbType = System.Data.SqlDbType.VarBinary;
            paramIn.Size = 128;
            paramIn.Direction = System.Data.ParameterDirection.Input;
            paramIn.Value = empStat.Salary;


            SqlParameter paramOut = new SqlParameter();
            paramOut.ParameterName = "@Salary";
            paramOut.SqlDbType = System.Data.SqlDbType.Decimal;
            paramOut.Precision = 10;
            paramOut.Scale = 2;
            paramOut.Value = 0.00;
            
            paramOut.Direction = System.Data.ParameterDirection.Output;


            procCommand.Parameters.Add(paramIn);
            procCommand.Parameters.Add(paramOut);

            procCommand.ExecuteScalar();

            empStat.ConvertedSalary = Decimal.Parse(paramOut.Value.ToString());

        }
    }
}
