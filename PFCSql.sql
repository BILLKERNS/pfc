﻿

--create database PFC


use PFC
truncate table EmployeeStats
go

drop table EmployeeStats
go

truncate table Employee
go

drop table Employee
go


create table Employee(PrimaryId bigint not null primary key identity(1,1), FirstName varchar(40) not null, LastName varchar(40) not null, Street1Address varchar(40) not null,
 Street2Address varchar(40) null, CityAddress varchar(30) not null, StateAddress varchar(2) not null, ZipAddress int not null, DateOfBirth DateTime2 not null);


 --Salary stored as an actual decimal(10,2), so eight leading digits and two decimals.
create table EmployeeStats(PrimaryId bigint not null primary key identity(1,1),EmployeeId bigint not null, Hired DateTime2 not null, Fired DateTime2 null, SupervisorId bigint null, SSN varbinary(128) not null, Salary varbinary(128) not null);

alter table EmployeeStats add foreign key(SupervisorId) references Employee(PrimaryId);
go

alter table EmployeeStats add foreign key(EmployeeId) references Employee(PrimaryId);
go

--Dummy Data
insert into Employee values('Bob', 'Lazern', '123 Main st.', null,'Yorkshire', 'PA', 17010, '1962-02-27 00:00:00')
go

insert into Employee values('Mike', 'Rothcoat', '329 SkyTown dr.','apt 23','MiddleRiver', 'PA', 17236, '1971-06-08 00:00:00')
go

insert into Employee values('Brenda', 'Tripollski', '56 Grove View way', null,'Harrisburg', 'PA', 170120236, '1983-08-07 00:00:00')
go

declare @SSN char(10)
set @SSN = '2563258951'
declare @EncSSN varbinary(128)
execute EncryptSSN @SSN, @EncSSN output
select @EncSSN

declare @Salary decimal(10,2)
set @Salary = 458000.23
declare @EncSalary varbinary(128)
execute EncryptSalary @Salary, @EncSalary output
select @EncSalary
insert into EmployeeStats values(1,'1982-03-08 00:00:00', null, null, @EncSSN, @EncSalary)
go

declare @SSN char(10)
set @SSN = '120569231'
declare @EncSSN varbinary(128)
exec EncryptSSN @SSN, @EncSSN output

declare @Salary decimal(10,2)
set @Salary = 53120.23
declare @EncSalary varbinary(128)
exec EncryptSalary @Salary, @EncSalary output
insert into EmployeeStats values(2,'1982-03-08 00:00:00', '1984-03-12 00:00:00', 1, @EncSSN, @EncSalary)
go


declare @SSN char(10)
set @SSN = '005020001'
declare @EncSSN varbinary(128)
exec EncryptSSN @SSN, @EncSSN output

declare @Salary decimal(10,2)
set @Salary = 60235.00
declare @EncSalary varbinary(128)
exec EncryptSalary @Salary, @EncSalary output
insert into EmployeeStats values(3,'1992-03-08 00:00:00', null, 1, @EncSSN, @EncSalary)
go



--Created Key Stuff
--straight from a google of MSDN

--IF NOT EXISTS 
--    (SELECT * FROM sys.symmetric_keys WHERE symmetric_key_id = 101)
--    CREATE MASTER KEY ENCRYPTION BY 
--    PASSWORD = 'AwHoLeBuNchOfHOOOOOOPla123@@!2'
--GO

--CREATE CERTIFICATE EmployeeSensativeCertificate
--   WITH SUBJECT = 'Employee SSN and Salary';
--GO

--CREATE SYMMETRIC KEY EmployeeStatsKey
--    WITH ALGORITHM = AES_256
--    ENCRYPTION BY CERTIFICATE EmployeeSensativeCertificate;
--GO

DROP PROCEDURE [dbo].[EncryptSSN]
GO


create procedure dbo.EncryptSSN @SSN char(10), @EncryptedSSN varbinary(128) out 
as
begin
OPEN SYMMETRIC KEY EmployeeStatsKey
   DECRYPTION BY CERTIFICATE EmployeeSensativeCertificate;
	  set @EncryptedSSN = EncryptByKey(Key_GUID('EmployeeStatsKey'), @SSN)
	  Close Symmetric Key EmployeeStatsKey
end
go

DROP PROCEDURE [dbo].[DecryptSSN]
GO

create procedure dbo.DecryptSSN @EncryptedSSN varbinary(128),@SSN char(10)  out
as
begin
OPEN SYMMETRIC KEY EmployeeStatsKey
   DECRYPTION BY CERTIFICATE EmployeeSensativeCertificate;
	 set @SSN = CONVERT(varchar(10), DecryptByKey(@EncryptedSSN))
	 Close Symmetric Key EmployeeStatsKey
end
go


--salary

DROP PROCEDURE [dbo].[EncryptSalary]
GO
create procedure dbo.EncryptSalary @Salary decimal(10,2), @EncryptedSalary varbinary(128) out
as
begin
OPEN SYMMETRIC KEY EmployeeStatsKey
   DECRYPTION BY CERTIFICATE EmployeeSensativeCertificate;
	  set @EncryptedSalary = EncryptByKey(Key_GUID('EmployeeStatsKey'), cast(@Salary as CHAR(10)))
	  Close Symmetric Key EmployeeStatsKey
end
go

DROP PROCEDURE [dbo].[DecryptSalary]
GO
create procedure dbo.DecryptSalary @EncryptedSalary varbinary(128),@Salary decimal(10,2) out
as
begin
OPEN SYMMETRIC KEY EmployeeStatsKey
   DECRYPTION BY CERTIFICATE EmployeeSensativeCertificate;
   declare @TempSalary char(10)
   set @TempSalary =   DecryptByKey(@EncryptedSalary)
  set @Salary = CONVERT(decimal(10,2), @TempSalary)
  select @TempSalary as 'Salary'
Close Symmetric Key EmployeeStatsKey
end
go

--end

drop procedure [dbo].[GetEmployee]
 go
create procedure GetEmployee @Id bigint = null
as
begin

	if(@Id is null)
	begin
		select * from Employee
	end
	else
		select * from Employee where Employee.PrimaryId = @Id
end
go


drop procedure [dbo].[GetEmployeeBySalary]
 go

create procedure GetEmployeeBySalary @Above decimal(10,2)
as
begin
OPEN SYMMETRIC KEY EmployeeStatsKey
   DECRYPTION BY CERTIFICATE EmployeeSensativeCertificate;

	select * from Employee e
	join EmployeeStats es on e.PrimaryId = es.SupervisorId
	where convert(decimal(10,2) ,DecryptByKey(es.Salary)) > @Above

	close symmetric key EmployeeStatsKey
end
go


